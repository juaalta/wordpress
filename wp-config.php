<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'basedatos' );

/** MySQL database username */
define( 'DB_USER', 'usuario' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password' );

/** MySQL hostname */
define( 'DB_HOST', 'database-1.cluster-cypvgae20wij.eu-west-3.rds.amazonaws.com' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '!qbKVe+&*~WUPD+;EW]V4,P37)7Y>GJ%{P=0N#@$Sf*o1Q>Q[cWS5t?kY4tu/f=@' );
define( 'SECURE_AUTH_KEY',  '_*v?ClJ#<jFTI1eJdGD7Xv,Ezs~G,_j22ONR0[)TL,VNR6(jy:&P_-KRl,kKjIZi' );
define( 'LOGGED_IN_KEY',    'K/LM$gNV|C?L}e3.m5-R+sa?9qQ8HwrLLW2*!%[}?wtb_U`c~^;I9Rg /!b9a2hB' );
define( 'NONCE_KEY',        '=`QPA{18xCD[oZk+F@4zTx-$DI{BD~r@.WWR#(BIh-]sHn4^gP,O.vJcbf~R_xPq' );
define( 'AUTH_SALT',        'QV6:]aqn/QuQDa-fK6?XUY(d2ViJ0$.(V]2Yrsgo*m}c2Cx6Q>-B!+P~bx6VGW)Y' );
define( 'SECURE_AUTH_SALT', 'DAsC;%+-/)<=j3%iW}~[/pY3j$?w7Uo#)ElxtN`Pk6TeFSAAa>UrT$Y]~bO=-$vd' );
define( 'LOGGED_IN_SALT',   'S,vL@PEtakvc]kg*1}~T4~BK=;H>)]f{D%?pe_laiq!x9oCe)}*okPtEcLbs,gZ.' );
define( 'NONCE_SALT',       ' sCfQB,7-AdQa+}(^`db/XD;[`JatCPrK2|kg*L6] 5IGI#+SXAo5[M2g}Jd[P a' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
